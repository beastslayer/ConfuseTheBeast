import yaml
import platform
import winreg
import os
import time

# TODO - use loguru to crate log files https://github.com/Delgan/loguru

def get_operating_system():
    """Get the operating system value.
    :return: the operating system value of the user."""

    # example return value: 'Windows-10-10.0.19043-SP0'
    p = platform.platform()
    #get value before the "-" delimiter
    os = p.split("-")[0]

    return os


def _get_windows_browser_path(browser_exe_name):
    """Find the browser executable path by querying the Windows registry.

     Keyword arguments:
     :param [browser_exe_name]:  [The browser name with the extension chosen by the user.]

     :return: the file path of the Windows browser executable.
     """

    registry_location = r'SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths'

    registry_path = os.path.join(registry_location, browser_exe_name)
    handle = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, registry_path)
    path = winreg.EnumValue(handle, 0)[1]

    return path


def update_browser_path(app_dict, browser, os):
    """Update the applicaiton dictionary with the browser executable path.

     Keyword arguments:
     :param [app_dict]:  [This is a application state dictionary.]
     :param [browser]: [The name of the browser the user wants to use with the program.]
     :param [os]: [The operating system of the computer.]

     :return: browser path.
     """

    if os == 'Windows':
        path = _get_windows_browser_path(app_dict, browser)
    elif os == 'Linux':
        print('not implemented yet')
    elif os == 'MacOS':
        print('not implemented yet')

    app_dict['computer'][browser]['exec_path'] = path

    return path

def save_config(app_dict, config_file_path):
    """Save the global application dictionary (data model) to yaml file in the config directory.

    Keyword arguments:
    :param [app_dict]:  [This is a application state dictionary.]
    :param [file_path]:  [The output yaml file path.]

    :return: Nothing is returned.
    """

    with open(config_file_path, 'w') as file:
        documents = yaml.dump(app_dict, file)

    return None


def load_config(config_file_path):
    """Load the global application yaml file as a python dictionary.

    Keyword arguments:
    :param [file_path]:  [This is the path to the configuration file.]

    :return: return the application configuration dictionary.
    """

    with open(config_file_path, 'r') as stream:
        try:
            d = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    return d

def update_data_model(dict, values_dict):
    """Update the global application dictionary (data model).

    Keyword arguments:
    :param [dict]:  [This is a dictionary wrapped in the scalpl library.]
    :param [values_dict:  [The dictionary returned by the Pyautogui window.Read() method]

    :return: Nothing is returned as this function updates a global dictionary
    """

    for key, value in values.items():
        if key == 0 or key.startswith('-'):
            pass
        else:
            dict[key] = value

    return None

def get_project_path(dir_name, file_name=None):
    dirname = os.path.dirname(os.path.abspath(__file__))
    parentDirectory = os.path.dirname(dirname)
    if file_name == None:
        dir = os.path.join(parentDirectory, dir_name)
    else:
        dir = os.path.join(parentDirectory, dir_name, file_name)
    return dir

