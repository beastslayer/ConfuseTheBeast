import time
import os
import random
import subprocess
import pyautogui as pag
import pygetwindow as pgw
import utilities as ut

# TODO - use library like nested_dict to make the dictionary lookups less brittle.
# TODO - use bezmouse library, or at least functions from library to get more realistic mouse movements.
# TODO - less reliance on keyboard shortcuts.

def cli(app_dict, browser, website, articles):
    # TODO - need to add the parameters to the app dict
    driver = _launch_browser(app_dict)
    driver.activate()
    driver.maximize()
    article_link_area = _get_article_link_area(app_dict, website)
    _browse_to_site(app_dict)
    category_queue = _create_menu_selection_queue(app_dict, articles)
    for category in category_queue:
        current_window = get_current_open_tab(browser)
        print(f' {current_window} starts with {category}')
        _click_web_pic(app_dict, category, current_window)
        time.sleep(3)
        _click_article(app_dict, article_link_area)
        time.sleep(3)
        print('Done with Click article, now going to read article')
        _read_article(app_dict, website)
        print('done reading article')
    print ('done!')

    return None

def _launch_browser(app_dict):
    """Launch a browser in a subprocess after closing all existing instances of the browser.

     Keyword arguments:
     :param [app_dict]:  [This is a application state dictionary.]

     :return: The focused browser window instance.
     """
    operating_system = app_dict['computer']['user_operating_system']
    website = app_dict['app']['website']
    browser = app_dict['app']['browser']
    browser_path = app_dict['computer']['browser'][browser]['exec_path']
    browser_new_tab = app_dict['websites'][website]['site_info']['new_tab_title']

    if browser_path == "":
        browser_path = ut.update_browser_path(app_dict, browser=browser, os=operating_system)
    else:
        pass
    _close_existing_browser_windows(browser)
    path = browser_path.encode('unicode_escape')
    # open browser
    subprocess.Popen(path)
    time.sleep(3)
    # focus on browser window
    driver = pgw.getWindowsWithTitle(browser_new_tab)[0]

    return driver


def _read_article(app_dict, website):
    browser = app_dict['app']['browser']
    reading_time = random.randint(5, 44)
    # TODO: would rather use mouse scroll than pagedown in future
    # scroll_amount = random.randint(100, 323)
    scroll_num = random.randint(1, 5)
    close_tab = app_dict['computer']['browser'][browser]['keys']['close_tab']

    time.sleep(2)
    article_loc_dict = _get_article_link_area(app_dict, website)
    pos = _get_in_position_to_find_article(article_loc_dict)
    time.sleep(1)
    # find an article to read.
    pag.moveTo(pos['x'], pos['y'], 2, pag.easeInBounce)
    for s in range(scroll_num):
        time.sleep(reading_time)
        pag.press('pagedown')
    # close tab and return to screen
    # TODO - get the hotkey in the dictionary
    pag.hotkey('ctrl', 'w')
    time.sleep(2)

    return None


def _browse_to_site(app_dict):
    url_list = _create_decoy_sites_queue(app_dict)

    for site in url_list:
        if site == url_list[-1]:
            _goto_site(app_dict, site)
        else:
            _goto_site(app_dict, site)
            _surf_decoy_site()

    return None


def _click_article(app_dict, article_link_area):
    current_tab_title = app_dict['app']['current_tab'].lower()
    browser = app_dict['app']['browser']
    x = article_link_area['horizontal_position']
    current_window = get_current_open_tab(browser)

    # Move mouse to middle of page towards the top (inline with links)
    _get_in_position_to_find_article(article_link_area)
    print(f'current window {current_window} starts with {current_tab_title}')
    # keep trying to open an article link.  once the link opens google news will not be in the window titles any longer
    # If it is then keep trying until an actual link is clicked.
    while current_window.startswith(current_tab_title):
        #get a random vertical mouse position (hopefully there is a link there)
        y = random.randint(article_link_area['first_article_vert'], article_link_area['last_article_vert'])
        pag.moveTo(x, y, 2, pag.easeOutQuad)
        pag.click(x, y)
        time.sleep(3)
        print(f'inside while - current window {current_window} starts with {current_tab_title}')
        current_window = get_current_open_tab(browser)
        print(f' new window is {current_window} compared to {current_tab_title}')

def _create_menu_selection_queue(app_dict, articles):
    """Create a list of picture paths related to the menus for a given site.

     Keyword arguments:
     :param [app_dict]:  [This is a application state dictionary.]

     :return: A list of menu picture names.
     """
    site = app_dict['app']['website']
    #articles = app_dict['app']['articles']
    link_dict = app_dict['websites'][site]['site_info']['link_menu_probabilities']

    link_list = list(link_dict.keys())
    weights = list(link_dict.values())

    link_selection_queue = random.choices(link_list, weights=weights, k=articles)

    return link_selection_queue


def _close_existing_browser_windows(browser):
    """Close all existing browser windows that the user selected for the program.

     Keyword arguments:
     :param [browser]:  [The browser selected as a parameter by the user.]

     :return: nothing returned.
     """
    window_list = pgw.getAllTitles()
    open_browsers = [window for window in window_list if window.endswith(browser)]

    for browser in open_browsers:
        b = pgw.getWindowsWithTitle(browser)[0]
        b.close()

    return None


def _click_web_pic(app_dict, pic_name, current_window):
    # TODO - use try to make this more resilient
    """Click on the screen element shown in the image.

    Keyword arguments:
    :pic_path: picture file path"""
    current_window = current_window.lower()
    pic_name = pic_name.lower()

    if current_window.startswith(pic_name):
        pass
    else:
        website = app_dict['app']['website']
        img_dir = app_dict['computer']['img_dir']
        img_name = pic_name + ".png"
        pic_path = os.path.join(img_dir, website, img_name)
        #locate the picture on the screen using opencv
        loc = pag.locateOnScreen(pic_path, grayscale=True, confidence=.7)
        btn_xy = pag.center(loc)
        pag.moveTo(btn_xy.x, btn_xy.y, 2, pag.easeOutQuad)
        pag.click(btn_xy.x, btn_xy.y)

    app_dict['app']['current_tab'] = pic_name

    return None


def _get_article_link_area(app_dict, website):
    """Get the coordinates of the area of the screen that contains article links.

    Keyword arguments:
    :param [app_dict]:  [This is a application state dictionary.]
    :param [website]: [The website to read.]

    :return: [Dictionary with the following keys, the values are pixels:
        - horizontal_position
        - first_article_vert
        - last_article_vert] """

    height = pag.size().height
    width = pag.size().width

    links_horizontal_offset = app_dict['websites'][website]['site_info']['link_loc']['links_horizontal_offset']
    first_article_vert_pct = app_dict['websites'][website]['site_info']['link_loc']['first_article_vert_pct']
    last_article_vert_pct = app_dict['websites'][website]['site_info']['link_loc']['last_article_vert_pct']

    d = {}
    d['horizontal_position'] = width * links_horizontal_offset
    d['first_article_vert'] = height * first_article_vert_pct
    d['last_article_vert'] = height * (1 - last_article_vert_pct)

    app_dict['app']['articles_loc'] = d

    return d


def _get_in_position_to_find_article(article_loc_dict):
    """Move mouse to around the area of the first article link and get in the horizontal position to navigate links.

    Keyword arguments:
    :param [article_loc_dict]: [The dictionary that contains screen location information]

    :return: [Dictionary of the x and y of the first article location in pixels]"""

    time.sleep(1)
    x = article_loc_dict['horizontal_position']
    y = article_loc_dict['first_article_vert']
    pag.moveTo(x, y, 2, pag.easeOutQuad)

    return {'x': x, 'y': y}


def _create_decoy_sites_queue(app_dict):
    """Create list of possible list of decoy sites before going to user selected site.

    Keyword arguments:
    :param [app_dict]:  [This is a application state dictionary.]

    :return: List of websites with user chosen being in the last position."""

    website = app_dict['app']['website']
    website_url = app_dict['websites'][website]['site_info']['url']
    decoy_dict = app_dict['websites']['Decoy']
    decoy_num = random.randint(1, 3)
    num = random.randint(0, 100)
    decoy_site_list = list(decoy_dict.keys())
    decoy_site_weights = list(decoy_dict.values())

    if num > 59:
        queue = random.choices(decoy_site_list, weights=decoy_site_weights, k=decoy_num)
        queue = list(set(queue))
    else:
        queue = []
    queue.append(website_url)

    return queue


# TODO: need to include the hotkey in the YAML file and get it to work. Will help cross platform efforts.
def _goto_address_bar(app_dict):
    # address_bar = app_dict['computer']['browser'][browser]['keys']['address_bar']
    pag.hotkey('ctrl', 'l')

    return None


def _goto_site(app_dict, url):

    t = random.randint(3, 10)
    _goto_address_bar(app_dict)
    pag.write(url)
    pag.press('enter')
    time.sleep(t)

    return None


# TODO: I would like this to have random movements, time spent, etc.  Also, need to accept all cookies in browser.
def _surf_decoy_site():

    t = random.randint(3, 10)
    time.sleep(t)
    pag.press('pagedown')
    t2 = random.randint(3, 10)
    time.sleep(t2)

    return None

def get_current_open_tab(browser):

    window_list = pgw.getAllTitles()
    open_tab = [window for window in window_list if window.endswith(browser)][0]

    return open_tab.lower()


"""import pyautogui
import time
import random as rnd

#calculate height and width of screen
w, h = list(pyautogui.size())[0], list(pyautogui.size())[1]

while True:
    time.sleep(1)
    #move mouse at random location in screen, change it to your preference
    pyautogui.moveTo(rnd.randrange(0, w), 
                     rnd.randrange(0, h))#, duration = 0.1)"""