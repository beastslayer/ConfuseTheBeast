import os
import utilities as ut
import driver

import PySimpleGUI as sg
from loguru import logger


logs_dir = ut.get_project_path('logs')
logger.add(os.path.join(logs_dir, "ctb_{time}.log"), rotation="3 days")

# save configuration
#
# default values need to be from app_dict


@logger.catch
def main():
    i = 0
    config_yaml = ut.get_project_path('config', 'beast_config.yaml')
    app_dict = ut.load_config(config_yaml)

    sg.theme('Topanga')

    # form = sg.FlexForm('Confuse The Beast')  # begin with a blank form
    info_layout = [[sg.Text('Short instructions on how to get the most from this program:', text_color='White')],
                   [sg.Text('Link to Video:', size=(40, 7))],
                   [sg.Text('Other ways YOU can battle the beast:', text_color='White')]]

    tab1_layout = [[sg.Text("What internet browser you want to use: ")],
                   [sg.InputOptionMenu(('Chrome', 'Edge'), size=(10, 3), default_value='Chrome',
                                       key='browser')],
                   [sg.Text("What website do you want to read?: ")],
                   [sg.InputOptionMenu(('Google', 'Reddit'), size=(10, 3),
                                       default_value='Google', key='website')],
                   [sg.Text("How many articles do you want to read?")],
                   [sg.InputText(6, size=(10, 3), key='articles', enable_events=True),
                    sg.Text('Estimated run time is: 30 Minutes', text_color='brown', key='-ARTICLES-')]]

    tab2_layout = [[sg.Text('Autorun time limit:')],
                   [sg.InputText(30, size=(10, 3), key='autorun', enable_events=True)]]

    action_layout = [[sg.Button('Run', size=(20, 2), key='-RUN-'),
                      sg.ProgressBar(1, orientation='h', size=(50, 30), key='-PROGRESS-')],
                     sg.Text('Program will autorun in: 30 seconds', text_color='brown', key='-AUTORUN-',
                             enable_events=True)]

    layout = [[sg.TabGroup([[sg.Tab('General', tab1_layout), sg.Tab('Advanced', tab2_layout)]]),
               sg.Frame('Key Info:', layout=info_layout, border_width=1)],
              action_layout]

    window = sg.Window('Confuse The Beast', layout)

    while True:
        event, values = window.read(timeout=1000)
        t = int(values['autorun'])
        countdown = t - i
        # See if user wants to quit or window was closed
        if event == sg.WINDOW_CLOSED:
            break
        elif event == 'articles':
            est = int(values['articles']) * 5
            window['-ARTICLES-'].update(f'Estimated run time is : {est} minutes')
        elif event == 'autorun':
            t = int(values['autorun'])
            app_dict['app']['countdown'] = int(values['autorun'])
        elif countdown > 0:
            window['-AUTORUN-'].update(f'Program will autorun in: {countdown} seconds')
            i += 1
        elif event == '-RUN-' or countdown == 0:
            browser = values['browser']
            print(browser)
            website = values['website']
            print(website)
            articles = int(values['articles'])
            print(website)
            driver.cli(app_dict, browser=browser, website=website, articles=articles)

        else:
            print(f'You need to add another branch for the event: "{event}"')

        # for i in range(1, 10000):
        #    sg.one_line_progress_meter('My Meter', i + 1, 10000, 'key', 'Optional message')
    window.close()


if __name__ == '__main__':
    main()

