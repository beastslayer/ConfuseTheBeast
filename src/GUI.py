import pprint as pp
import platform
import PySimpleGUI as sg
from scalpl import Cut
import utilities as ut
import time


#### VARIABLES
c = r'C:\Users\benja\PycharmProjects\confuse_the_beast\beast_config.yaml'
app_dict = ut.load_config(c)
########################################################################################################################
app = Cut(app_dict)
user_operating_system_path = 'computer.user_operating_system'
browser_path_path = str(f'computer.browser_path.{app.get(user_operating_system_path)}.exe_path')
articles_path = 'app.articles'
website_path = 'app.website'
browser_path = 'app.browser'
articles_path = 'app.articles'

########################################################################################################################
#### GUI FUNCTIONS
########################################################################################################################

def update_data_model(dict, values_dict):
    """Update the global application dictionary (data model).

    Keyword arguments:
    :param [dict]:  [This is a dictionary wrapped in the scalpl library.]
    :param [values_dict:  [The dictionary returned by the Pyautogui window.Read() method]

    :return: Nothing is returned as this function updates a global dictionary
    """

    for key, value in values.items():
        if key == 0 or key.startswith('-'):
            pass
        else:
            dict[key] = value

    return None



########################################################################################################################
#### GUI
########################################################################################################################


sg.theme('Topanga')

#form = sg.FlexForm('Confuse The Beast')  # begin with a blank form
info_layout = [[sg.Text('Short instructions on how to get the most from this program:', text_color='White')],
               [sg.Text('Link to Video:', size=(40,7))],
               [sg.Text('Other ways YOU can battle the beast:', text_color='White')]]

tab1_layout = [[sg.Text("What internet browser you want to use: ")],
               [sg.InputOptionMenu(('Chrome', 'Edge'), size=(10,3), default_value=app.get(browser_path), key= browser_path)],
               [sg.Text("What website do you want to read?: ")],
               [sg.InputOptionMenu(('Google', 'Reddit'), size=(10, 3),
                                   default_value= app.get(website_path), key=website_path)],
               [sg.Text("How many articles do you want to read?")],
               [sg.InputText(app.get(articles_path), size=(10,3), key=articles_path, enable_events=True),
                sg.Text('Estimated run time is: 30 Minutes', text_color='brown', key='-ARTICLES-')]]

tab2_layout = [[sg.Text('Autorun time limit:')],
               [sg.InputText(30, size=(10,3), key='autorun', enable_events=True),
                sg.Text('Program will autorun in: 30 seconds', text_color='brown', key='-AUTORUN-', enable_events=True)]]


action_layout = [[sg.Button('Run', size=(20,2), key= '-RUN-'),
                  sg.ProgressBar(1, orientation='h', size=(35, 20), key='-PROGRESS-')]]

layout = [[sg.TabGroup([[sg.Tab('General', tab1_layout), sg.Tab('Advanced', tab2_layout)]]),
           sg.Frame('Key Info:', layout=info_layout, border_width=1)],
          action_layout]
window = sg.Window('Confuse The Beast', layout)

i = 0
while True:
    event, values = window.read(timeout=1000)
    t = int(values['autorun'])
    countdown = t - i
    # See if user wants to quit or window was closed
    if event == sg.WINDOW_CLOSED:
        break
    elif countdown > 0:
        window['-AUTORUN-'].update(f'Program will autorun in: {countdown} seconds')
        i += 1
    elif countdown == 0:
        print('DONE!')
    elif event == articles_path:
        est = int(window[articles_path].get()) * 5
        window['-ARTICLES-'].update(f'Estimated run time is : {est} minutes')
    #elif event == '-AUTORUN-':

    #elif event == 'autorun':

    elif event == '-RUN-':
        update_data_model(app, values)
        print(values)
        pp.pprint(app_dict)
    else:
        print(f'You need to add another branch for the event: "{event}"')

    #for i in range(1, 10000):
    #    sg.one_line_progress_meter('My Meter', i + 1, 10000, 'key', 'Optional message')
window.close()


